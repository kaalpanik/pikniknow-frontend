import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/authentication/login/login.component';
import { AuthGuard } from './domain/service/auth-guard-service';
import { HomeComponent } from './home/home.component';

import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { UserModule } from './component/user/user.module';
import { DashboardComponent } from './component/authentication/dashboard/dashboard.component';
import { RegistrationComponent } from './component/authentication/registration/registration.component';
import { VerificationComponent } from './component/authentication/verfication/verification.component';


const routes: Routes = [
  { path: "user", loadChildren: './component/user/user.module#UserModule', canActivate: [AuthGuard], data: { pageName: 'User' } },
  { path: "story", loadChildren: './component/story/story.module#StoryModule', canActivate: [AuthGuard], data: { pageName: 'Story' } },
  { path: "banner", loadChildren: './component/banner/banner.module#BannerModule', canActivate: [AuthGuard], data: { pageName: 'Banner' } },
  { path: "page", loadChildren: './component/page/page.module#PageModule', canActivate: [AuthGuard], data: { pageName: 'Page' } },
  { path: 'login', component: LoginComponent, data: { pageName: 'Login' } },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], data: { pageName: 'Dashboard' } },
  { path: 'registration', component: RegistrationComponent, data: { pageName: 'Registration' } },
  { path: 'forgot-password', component: VerificationComponent, data: { pageName: 'Verification' } },
  { path: '', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard], data: { pageName: 'Dashboard' }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash:true })],
  // imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
