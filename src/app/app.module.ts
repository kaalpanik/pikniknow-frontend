import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';

import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { SideBarComponent } from './component/shared/side-bar/side-bar.component';
import { FooterBarComponent } from './component/shared/footer-bar/footer-bar.component';
import { TopBarComponent } from './component/shared/top-bar/top-bar.component';
import { BreadcrumbComponent } from './component/shared/breadcrumb/breadcrumb.component';
import { BraodcastService } from './domain/service/broadcast-service';
import { StorageService } from './domain/service/storage-service';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators'
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './component/authentication/login/login.component';
import { DashboardComponent } from './component/authentication/dashboard/dashboard.component';
import { AuthGuard } from './domain/service/auth-guard-service';
import { RegistrationComponent } from './component/authentication/registration/registration.component';
import { RxHttp } from '@rxweb/http';
import { NotificationService } from './domain/service/notification/notificaiton-service';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { VerificationComponent } from './component/authentication/verfication/verification.component';
import { ConfirmDialogModule } from './domain/service/dialog/confirm-dialog-module';
import { ConfirmDialogComponent } from './domain/service/dialog/confirm-dialog.component';
import { SignalRService } from './domain/service/signal-r.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { UserAddComponent } from './component/user/user-add/user-add.component';
import { UserModule } from './component/user/user.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    FetchDataComponent,
    SideBarComponent,
    FooterBarComponent,
    TopBarComponent,
    BreadcrumbComponent,
    LoginComponent,
    DashboardComponent,
    RegistrationComponent,
    VerificationComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    AppRoutingModule,
    AngularDualListBoxModule,
    ConfirmDialogModule,
    BrowserAnimationsModule,
    MatDialogModule,
    UserModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },AuthGuard, SignalRService,StorageService, BraodcastService, RxHttp, NotificationService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
