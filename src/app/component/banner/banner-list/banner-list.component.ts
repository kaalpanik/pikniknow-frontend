import { Component, OnInit } from '@angular/core';
import { BannerDomain } from '../banner-domain';
import { RxHttp } from '@rxweb/http';
import { NotificationService } from 'src/app/domain/service/notification/notificaiton-service';
import { APIResponseViewModel } from 'src/app/view-model/api-response-view-model';

@Component({
  selector: 'app-banner-list',
  templateUrl: './banner-list.component.html',
  styleUrls: ['./banner-list.component.css']
})
export class BannerListComponent extends BannerDomain implements OnInit {

  bannerList: any[] = [];
  constructor(private rxHttp: RxHttp,
    notificationService: NotificationService
  ) {
    super();
    this.notificationService = notificationService;
  }

  ngOnInit() {
    this.subscription.push(this.rxHttp.get({ path: "banner" }).subscribe((response: APIResponseViewModel) => {
      this.bannerList = response.response;
    }))
  }


  deleteBanner(bannerId: number) {
    this.subscription.push(this.rxHttp.delete({ path: "banner", params: [bannerId], body: null }).subscribe((response: APIResponseViewModel) => {
      this.bannerList = response.response;
      this.notificationService.success("Banner Deleted");
      this.ngOnInit();
    }))
  }
}
