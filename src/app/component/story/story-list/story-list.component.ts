import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { RxHttp } from '@rxweb/http';
import { APIResponseViewModel } from 'src/app/view-model/api-response-view-model';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/domain/service/notification/notificaiton-service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.css']
})
export class StoryListComponent implements OnInit, OnDestroy {
  story: any[] = []
  showComponent: boolean = false;
  subscription: Subscription;
  search: string = "";
  totalStory: number = 0;
  pageIndex = 1;
  rowCount = 10;
  statusId: number;
  loadedAll = false;
  searchFreeText: any = {
    "searchFreeText": "",
    "loginUserId": 0,
    "isAdmin": 1,
    "statusId": "",
    "date": ""
  };
  constructor(private rxHttp: RxHttp,
    private route: ActivatedRoute,
    private notificationService: NotificationService, ) {

    this.route.queryParams.subscribe(params => {
      if (params['typeId']) {
        this.statusId = params['typeId'];
        this.filterValue(this.statusId, 'statusId')
      }
    });

    this.rxHttp.badRequest = (errorMessage: any) => {
      let data = document.getElementById("loader");
      data.classList.remove("loading")
      this.notificationService.error(errorMessage);
    }
  }

  ngOnInit() {
    this.bindStory();
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  bindStory() {
    this.subscription = this.rxHttp.get({
      path: "story",
      queryParams:
        { userId: 0, searchQuery: JSON.stringify(this.searchFreeText), orderByColumn: "", sortOrder: "", pageIndex: this.pageIndex, rowCount: this.rowCount, storyId: 0 }
    }).subscribe((res: APIResponseViewModel) => {
      if (res.response) {
        let story = JSON.parse(res.response);
        if (story.length > 0) {
          story.forEach(obj => {
            this.story.push(obj)
          })
          this.totalStory = this.story[0].totalCount;
        }
      }
      this.hideLoadMoreButtom(this.pageIndex, this.rowCount, this.totalStory);
      this.showComponent = true;

    });
  }

  filterValue(value: any, type: any) {
    this.searchFreeText[type] = value;
    this.searchFilter();
  }

  @HostListener("window:scroll", [])
  onScroll(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      if (this.loadedAll) {
        this.pageIndex += 1;
        this.bindStory();

      }
    } else if ((window.innerHeight + window.scrollY) < document.body.offsetHeight) {
    }
  }

  detectBottom(): void {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      if (this.loadedAll) {
        this.pageIndex += 1;
        this.bindStory();
      }
    }
  }

  hideLoadMoreButtom(pageNumber: number, rowCount: number, totalCount: number): void {
    let data = pageNumber * rowCount;
    this.loadedAll = (pageNumber * rowCount < totalCount);
  }

  manageStory(story: any, statusId: number, message: string) {
    let body = {
      storyId: story.storyId,
      statusId: statusId
    }
    this.subscription = this.rxHttp.post({ path: "story/UpdateStory", body: body }).subscribe((data: APIResponseViewModel) => {
      this.notificationService.success(message);
      this.searchFilter();
    })
  }

  searchFilter() {
    this.pageIndex = 1;
    this.subscription = this.rxHttp.get({
      path: "story",
      queryParams:
        { userId: 0, searchQuery: JSON.stringify(this.searchFreeText), orderByColumn: "", sortOrder: "", pageIndex: this.pageIndex, rowCount: this.rowCount, storyId: 0 }
    }).subscribe((res: APIResponseViewModel) => {
      if (res.response) {
        this.loadedAll = true;
        this.story = JSON.parse(res.response);
        if (this.story.length > 0) {
          this.totalStory = this.story[0].totalCount;
        }
        else {
          this.totalStory = 0;
        }
      }
      this.showComponent = true;
    });
  }

  reset() {
    this.searchFreeText = {
      "searchFreeText": "",
      "loginUserId": 0,
      "isAdmin": 1,
      "statusId": "",
      "date": ""
    }
    this.searchFilter();
  }
}
