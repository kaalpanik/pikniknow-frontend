import { Component, OnInit } from '@angular/core';
import { RxHttp } from '@rxweb/http';
import { PageDomain } from '../page-domain';
import { APIResponseViewModel } from 'src/app/view-model/api-response-view-model';
import { NotificationService } from 'src/app/domain/service/notification/notificaiton-service';

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.css']
})
export class PageListComponent extends PageDomain implements OnInit {

  pageList: any[] = [];
  constructor(private rxHttp: RxHttp,
    private notificationService: NotificationService
  ) { super(); }

  ngOnInit() {
    this.subscription.push(this.rxHttp.get({ path:"page" }).subscribe((response: APIResponseViewModel) => {
      this.pageList = response.response;
    }))
  }


  deletePage(pageId: number) {
    this.subscription.push(this.rxHttp.delete({ path: "page", params: [pageId], body:null }).subscribe((response: APIResponseViewModel) => {
      this.pageList = response.response;
      this.notificationService.success("Page Deleted");
      this.ngOnInit();
    }))
  }
}
